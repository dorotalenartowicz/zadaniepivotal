﻿namespace Pivotal
{
    partial class Formularz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxStawkaVat = new System.Windows.Forms.ComboBox();
            this.textBoxKwotaNetto = new System.Windows.Forms.TextBox();
            this.buttonOblicz = new System.Windows.Forms.Button();
            this.textBoxKwotaBrutto = new System.Windows.Forms.TextBox();
            this.buttonWyczysc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kwota netto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(175, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stawka VAT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(420, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kwota brutto";
            // 
            // comboBoxStawkaVat
            // 
            this.comboBoxStawkaVat.FormattingEnabled = true;
            this.comboBoxStawkaVat.Location = new System.Drawing.Point(178, 69);
            this.comboBoxStawkaVat.Name = "comboBoxStawkaVat";
            this.comboBoxStawkaVat.Size = new System.Drawing.Size(78, 24);
            this.comboBoxStawkaVat.TabIndex = 3;
            // 
            // textBoxKwotaNetto
            // 
            this.textBoxKwotaNetto.Location = new System.Drawing.Point(18, 71);
            this.textBoxKwotaNetto.Name = "textBoxKwotaNetto";
            this.textBoxKwotaNetto.Size = new System.Drawing.Size(140, 22);
            this.textBoxKwotaNetto.TabIndex = 4;
            // 
            // buttonOblicz
            // 
            this.buttonOblicz.Location = new System.Drawing.Point(18, 126);
            this.buttonOblicz.Name = "buttonOblicz";
            this.buttonOblicz.Size = new System.Drawing.Size(121, 33);
            this.buttonOblicz.TabIndex = 5;
            this.buttonOblicz.Text = "Oblicz";
            this.buttonOblicz.UseVisualStyleBackColor = true;
            this.buttonOblicz.Click += new System.EventHandler(this.buttonOblicz_Click);
            // 
            // textBoxKwotaBrutto
            // 
            this.textBoxKwotaBrutto.Location = new System.Drawing.Point(423, 71);
            this.textBoxKwotaBrutto.Name = "textBoxKwotaBrutto";
            this.textBoxKwotaBrutto.ReadOnly = true;
            this.textBoxKwotaBrutto.Size = new System.Drawing.Size(140, 22);
            this.textBoxKwotaBrutto.TabIndex = 6;
            // 
            // buttonWyczysc
            // 
            this.buttonWyczysc.Location = new System.Drawing.Point(423, 126);
            this.buttonWyczysc.Name = "buttonWyczysc";
            this.buttonWyczysc.Size = new System.Drawing.Size(121, 33);
            this.buttonWyczysc.TabIndex = 7;
            this.buttonWyczysc.Text = "Wyczyść";
            this.buttonWyczysc.UseVisualStyleBackColor = true;
            this.buttonWyczysc.Click += new System.EventHandler(this.buttonWyczysc_Click);
            // 
            // Formularz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 242);
            this.Controls.Add(this.buttonWyczysc);
            this.Controls.Add(this.textBoxKwotaBrutto);
            this.Controls.Add(this.buttonOblicz);
            this.Controls.Add(this.textBoxKwotaNetto);
            this.Controls.Add(this.comboBoxStawkaVat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Formularz";
            this.Text = "Kalkulator netto-brutto";
            this.Load += new System.EventHandler(this.Formularz_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxStawkaVat;
        private System.Windows.Forms.TextBox textBoxKwotaNetto;
        private System.Windows.Forms.Button buttonOblicz;
        private System.Windows.Forms.TextBox textBoxKwotaBrutto;
        private System.Windows.Forms.Button buttonWyczysc;
    }
}

