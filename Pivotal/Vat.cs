﻿namespace Pivotal
{
    public class Vat
    {
        public string Opis { get; set; }
        public double Wartosc { get; set; }

        public Vat(string opis, double wartosc)
        {
            Opis = opis;
            Wartosc = wartosc;
        }
    }
}
