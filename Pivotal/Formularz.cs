﻿using System.Windows.Forms;

namespace Pivotal
{
    public partial class Formularz : Form
    {
        public Formularz()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Metoda wyliczająca i ustawiająca kwotę brutto
        /// </summary>
        private void buttonOblicz_Click(object sender, System.EventArgs e)
        {

            if (!double.TryParse(textBoxKwotaNetto.Text, out var kwotaNetto))
            {
                MessageBox.Show("Niepoprawna kwota netto", "Błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                var kwotaBrutto = Kalkulator.ObliczKwoteBrutto(kwotaNetto, ((Vat)comboBoxStawkaVat.SelectedItem).Wartosc);
                textBoxKwotaBrutto.Text = kwotaBrutto.ToString("0.##");
            }

        }

        /// <summary>
        /// Metoda czyści dane formularza
        /// </summary>
        private void buttonWyczysc_Click(object sender, System.EventArgs e)
        {
            textBoxKwotaNetto.Text = string.Empty;
            comboBoxStawkaVat.SelectedItem = null;
            textBoxKwotaBrutto.Text = string.Empty;

        }

        private void Formularz_Load(object sender, System.EventArgs e)
        {
            this.comboBoxStawkaVat.DataSource = new BindingSource(Kalkulator.StawkiVat, null);
            this.comboBoxStawkaVat.DisplayMember = "Opis";
            this.comboBoxStawkaVat.ValueMember = "Wartosc";
            this.comboBoxStawkaVat.DropDownStyle = ComboBoxStyle.DropDownList;
        }
    }
}
