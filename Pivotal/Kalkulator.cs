﻿using System.Collections.Generic;

namespace Pivotal
{
    public static class Kalkulator
    {
        /// <summary>
        /// Lista możliwych do wybrania stawek VAT
        /// </summary>
        public static readonly List<Vat> StawkiVat = new List<Vat>
        {
            new Vat ("8%",0.08),
            new Vat ("23%",0.23)
        };

        public static double ObliczWartoscVat(double kwotaNetto, double stawkaVat)
        {
            return kwotaNetto * stawkaVat;
        }


        public static double ObliczKwoteBrutto(double kwotaNetto, double stawkaVat)
        {
            return kwotaNetto + ObliczWartoscVat(kwotaNetto, stawkaVat);

        }
    }
}
